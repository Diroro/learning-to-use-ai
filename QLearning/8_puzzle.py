import search
from typing import List
import gym
import numpy as np
import random

class Problem(object):

    """The abstract class for a formal problem. You should subclass
    this and implement the methods actions and result, and possibly
    __init__, goal_test, and path_cost. Then you will create instances
    of your subclass and solve them with the various search functions."""

    def __init__(self, initial, goal=None):
        """The constructor specifies the initial state, and possibly a goal
        state, if there is a unique goal. Your subclass's constructor can add
        other arguments."""
        self.initial = initial
        self.goal = goal

    def actions(self, state):
        """Return the actions that can be executed in the given
        state. The result would typically be a list, but if there are
        many actions, consider yielding them one at a time in an
        iterator, rather than building them all at once."""
        raise NotImplementedError

    def result(self, state, action):
        """Return the state that results from executing the given
        action in the given state. The action must be one of
        self.actions(state)."""
        raise NotImplementedError

    def goal_test(self, state):
        """Return True if the state is a goal. The default method compares the
        state to self.goal or checks for state in self.goal if it is a
        list, as specified in the constructor. Override this method if
        checking against a single self.goal is not enough."""
        if isinstance(self.goal, list):
            return is_in(state, self.goal)
        else:
            return state == self.goal

    def path_cost(self, c, state1, action, state2):
        """Return the cost of a solution path that arrives at state2 from
        state1 via action, assuming cost c to get up to state1. If the problem
        is such that the path doesn't matter, this function will only look at
        state2.  If the path does matter, it will consider c and maybe state1
        and action. The default method costs 1 for every step in the path."""
        return c + 1

    def value(self, state):
        """For optimization problems, each state has a value.  Hill-climbing
        and related algorithms try to maximize this value."""
        raise NotImplementedError

    def coste_de_aplicar_accion(self, estado, accion):
        """Respecto a la versiòn original de AIMA hemos incluido está función que devuelve el coste de un único operador (aplicar accion a estado). Por defecto, este
        coste es 1. Reimplementar si el problema define otro coste """ 
        return 1

class Ocho_Puzzle(Problem):

    def __init__(self, initial, goal=(1, 2, 3, 4, 5, 6, 7, 8, 0)):
        """ Define goal state and initialize a problem """
        self.goal = goal
        Problem.__init__(self, initial, goal)
    
    def actions(self,estado):
        pos_hueco=estado.index(0) # busco la posicion del 0
        accs=list()
        if pos_hueco not in (0,1,2):
            accs.append(0)
        if pos_hueco not in (2,5,8):
            accs.append(2)
        if pos_hueco not in (0,3,6):
            accs.append(3)
        if pos_hueco not in (6,7,8):
            accs.append(1) 
        return accs     

    def result(self,estado,accion):
        pos_hueco = estado.index(0)
        l = list(estado)
        if accion == "Mover hueco arriba" or accion ==0:
            l[pos_hueco] = l[pos_hueco-3]
            l[pos_hueco-3] = 0
        elif accion == "Mover hueco abajo" or accion == 1:
            l[pos_hueco] = l[pos_hueco+3]
            l[pos_hueco+3] = 0
        elif accion == "Mover hueco derecha" or accion == 2:
            l[pos_hueco] = l[pos_hueco+1]
            l[pos_hueco+1] = 0
        elif accion == "Mover hueco izquierda" or accion == 3:
            l[pos_hueco] = l[pos_hueco-1]
            l[pos_hueco-1] = 0
        return tuple(l)
    
    def h(self, state):  #Heuristica lineal para calcular reward
        lnr = 0
        for x in state:
            if(state.index(x) != self.goal.index(x)):
                lnr += 1
        return lnr

    def check_solvability(self, state):
        inversion = 0
        for i in range(len(state)):
            for j in range(i+1, len(state)):
                if (state[i] > state[j]) and state[i] != 0 and state[j]!= 0:
                    inversion += 1
        return inversion % 2 == 0 
    
    observation_space = len(dic)
    action_space = 4
    state_cod = "[Mover hueco arriba == 0, Mover hueco abajo == 1, Mover hueco derecha == 2, Mover hueco izquierda == 3]"
    def reset(self):
        t = [0,1,2,3,4,5,6,7,8]
        random.shuffle(t)
        return tuple(t)

#Auxiliary functions
import itertools
#Para poder utilizar la QTable debemos poder codificar los estados
#Se creará un diccionario que contenga todas las permutaciones posibles de la tupla (0,1,2,3,4,5,6,7,8) y devuleva un valor int
#Esto nos permitira buscar en tiempo en O(1) la codficiación del estado sin influir en el rendimiento del algoritmo
l = itertools.permutations(range(9))
dic = {}
for i, x in enumerate(l):
    dic[x] = is

#Solving the problem
#Parameters
num_episodes = 4000
gamma = 0.8
alpha = 0.5
epsilon = 0.6
total_penalties, total_epochs = 0, 0

#solving function
def main():
    env = Ocho_Puzzle((0,1,2,3,4,5,6,7,8))
    rewards = []
    
    Q = np.zeros((env.observation_space, env.action_space))

    print("Action Space {}".format(env.action_space))
    print("State Space {}".format(env.observation_space))
    print("Codificacion de estados {}".format(env.state_cod))
    
    for episode in range(1, num_episodes + 1):
        state = env.reset() #Estado inicial aleatorio
        done = False
        while not done:
            if random.uniform(0, 1) < epsilon:
                action = random.choice(env.actions(state)) #Dentro de las acciones posibles toma una al azar
            else:
                action = np.argmax(Q[dic.get(state)])
            try:
                state2 = env.result(state, action)  #El siguiente esatdo es el resultado de la defincion de Ocho_Puzzle.result(state, action)
                reward = (env.h(state) - env.h(state2))*10   #Reward es la diferencia de valores de heuristica lineal de los estados *10
            except:
                state2 = state                      #Si la accion es ilegal vuelve a probar con el mismo estado otro movimiento
                reward = -10
            done = env.goal_test(state2)    #Done si state2 == goal
            old_value = Q[dic.get(state), action]
            next_max = np.max(Q[dic.get(state2)])

            new_value = (1 - alpha) * old_value + alpha * (reward + gamma * next_max)
            Q[dic.get(state), action] = new_value

            state = state2

        if episode % 400 == 0:
            print(f"Results after {episode} episodes:")
            total_epochs, total_penalties = 0, 0
            for j in range(30):
                state = env.reset()     ##Tomamos un estado aleatorio
                epochs, penalties, reward = 0, 0, 0
                done = False
                while not done:     ##Probamos hasta encontrar una solución
                    if random.uniform(0, 1) < 0.3:  #Modificamos epsilon a un valor mas bajo para que aporveche más la matriz en las pruebas
                        action = random.choice(env.actions(state)) 
                    else:
                        action = np.argmax(Q[dic.get(state)])
                    try:
                        state2 = env.result(state, action)
                        reward = (env.h(state) - env.h(state2))*10
                    except:
                        state2 = state
                        reward = -10
                    done = env.goal_test(state2)
                    if reward == -10:
                        penalties += 1
                    epochs += 1
                    state = state2
                total_epochs += epochs
                total_penalties += penalties
            
            print(f"Average timesteps per episode: {total_epochs / j}")
            print(f"Average penalties per episode: {total_penalties / j}")
    print("Fin del entrenamiento")

main()  #Llamamos para resolver el problema

#Se puede observar que la busqueda A* es mas eficiente que la busqueda por refuerzo previa al entrenamiento puesto que esta es equivalente a una busqueda ciega.
#Conforme se llena la matriz Q se reduce el numero de episodios necesarios para encontrar una solución. Esta diferencia se hace mas notable si se reduce el valor
#de los hiperparametros porque exploramos menos y explotamos mas los valores que se saben optimos.
#Tarda unos 25 min en sacar los resultados de los 4000 episodios
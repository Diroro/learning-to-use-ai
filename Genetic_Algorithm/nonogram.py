import search
import numpy as np
import random
import time
import os

class ProblemaGenetico(object):
        def __init__(self, genes,fun_dec,fun_muta , fun_cruza, fun_fitness,longitud_individuos, goal):
            self.genes = genes
            self.fun_dec = fun_dec
            self.fun_cruza = fun_cruza
            self.fun_muta = fun_muta
            self.fun_fitness = fun_fitness
            self.longitud_individuos = longitud_individuos
            self.goal = goal
            """Constructor de la clase"""
        def decodifica(self, genotipo):
            """Devuelve el fenotipo a partir del genotipo"""
            fenotipo = self.fun_dec(genotipo)
            return fenotipo
        def muta(self, cromosoma,prob):
            """Devuelve el cromosoma mutado"""   
            mutante = self.fun_muta(cromosoma,prob)
            return mutante
        def cruza(self, cromosoma1, cromosoma2):         
            """Devuelve el cruce de un par de cromosomas"""
            cruce = self.fun_cruza(cromosoma1,cromosoma2)
            return cruce 
        def fitness(self, cromosoma):    
            """Función de valoración"""
            valoracion = self.fun_fitness(cromosoma, self.goal)
            return valoracion


#Definición del problema nonograma
def fun_cruzar_linea(cromosoma1, cromosoma2):
    l1 = len(cromosoma1)
    l2 = len(cromosoma2)
    cruce1 = cromosoma1[0:l1//2]+cromosoma2[l1//2:l2]
    cruce2 = cromosoma2[0:l2//2]+cromosoma1[l2//2:l1]
    return [cruce1,cruce2]

def fun_cruzar_matriz(cromosoma1, cromosoma2):
    m = [[],[]]
    for i in range(len(cromosoma1)):
        l = fun_cruzar_linea(cromosoma1[i], cromosoma2[i])
        m[0].append(l[0])
        m[1].append(l[1])
    return m

def fun_mutar(cromosoma,prob):
    l = len(cromosoma)
    p = random.randint(0,l-1)   #cuidado si no es cuadrada la matriz esto está mal
    for i in range(l):
        if prob > random.uniform(0,1):
            cromosoma[i][p] = (cromosoma[i][p]+1)%2
    return cromosoma

def check_rest(sec, restrict):          #Imita una maquina de estados finita
    act = True                          #Esta variable indica en que estado estamos en la FSM (S0 = !act y S1 = act)
    i,j,count = 0, 0, 0
    while i < len(sec) and sec[i] != 1: #Mientras haya cruces avanza
        i+=1
    if i == len(sec):                   #Todo cruces solo es valido si la restriccion es 0
        if restrict[0] == 0:
            return 1
        else:
            return 0
    while i < len(sec) and j < len(restrict):
        if (not act) and sec[i] == 1:   #S0 -> S1
            act = True
            count = 1
        elif act and sec[i] == 1:       #S1 -> S1
            count+=1
        elif act and sec[i] == 0:       #S1 -> S0
            if count == restrict[j]:    #Se cumple la restricción
                j+=1                    #Comprobamos la siguiente
                count = 0
                act = False
            else:
                return 0
        if j < len(restrict) and count > restrict[j]:
            return 0                    #La secuencia de cuadrados es mayor que la restriccion
        i+=1                            #S0 -> S0
    if j < len(restrict):
        if count == restrict[j]:
            return 1                    #Justo se cumple la ultima restriccion
        else:
            return 0                    #Quedan restricciones por cumplir
    if i < len(sec):
        for k in range(len(sec) - i):
            if sec[i+k] == 1:
                return 0                #No quedan restricciones pero quedan cuadrados
    return 1                    #Con un breakpoint aqui se puede comprobar que funciona

def fun_dec(cromosoma):
    return cromosoma

def fun_fitness_nono(cromosoma, restrict):
    n = 0
    for i in range(len(cromosoma)):
        col = []
        for j in range(len(cromosoma[i])):
            col.append(cromosoma[j][i])
        n+=check_rest(cromosoma[i], restrict[0][i])
        n+=check_rest(col, restrict[1][i])
    return n


#Algoritmo genético
def muta_individuos(problema_genetico, poblacion, prob):
    l = []
    for i in range(len(poblacion)):
        l.append(problema_genetico.muta(poblacion[i], prob))
    return l

def seleccion_por_torneo(problema_genetico, poblacion, n, k, opt):
    seleccionados = []
    for i in range(n):
        participantes = random.sample(poblacion,k)
        seleccionado = opt(participantes, key=problema_genetico.fitness)
        opt(poblacion, key=problema_genetico.fitness)
        seleccionados.append(seleccionado)
    return seleccionados

def cruza_padres(problema_genetico,padres):
    l = []
    for i in range(len(padres)//2):
        desc = problema_genetico.fun_cruza(padres[2*i],padres[2*i+1])
        l.append(desc[0])
        l.append(desc[1])
    return l

def poblacion_inicial(problema_genetico, size):
    l=[]
    for _ in range(size):
        x=[]
        for _ in range(problema_genetico.longitud_individuos):
            f = []
            for _ in range(problema_genetico.longitud_individuos):
                f.append(random.choice(problema_genetico.genes))
            x.append(f)
        l.append(x) 
    return l

def nueva_generacion(problema_genetico, k,opt, poblacion, n_padres, n_directos, prob_mutar):
    padres2 = seleccion_por_torneo(problema_genetico, poblacion, n_directos, k,opt) 
    padres1 = seleccion_por_torneo(problema_genetico, poblacion, n_padres , k, opt)
    cruces =  cruza_padres(problema_genetico,padres1)
    generacion = padres2+cruces
    resultado_mutaciones = muta_individuos(problema_genetico, generacion, prob_mutar)
    return resultado_mutaciones

# k: número de participantes en los torneos de selección.
# opt: max ó min, dependiendo si el problema es de maximización o de minimización. 
# nGen: número de generaciones (que se usa como condición de terminación)
# size: número de individuos en cada generación
# prop_cruce: proporción del total de la población que serán padres. 
# prob_mutación: probabilidad de realizar una mutación de un gen.
def algoritmo_genetico(problema_genetico,k,opt,ngen,size,prop_cruces,prob_mutar):
    poblacion = poblacion_inicial(problema_genetico,size)
    n_padres=round(size*prop_cruces)
    n_padres= int (n_padres if n_padres%2==0 else n_padres-1)
    n_directos = size-n_padres
    for i in range(ngen):
        poblacion= nueva_generacion(problema_genetico,k,opt,poblacion,n_padres, n_directos,prob_mutar)
        print(f"\rGeneration {i+1}", end="")
    mejor_cr= opt(poblacion, key=problema_genetico.fitness)
    mejor=problema_genetico.decodifica(mejor_cr)
    return (mejor,problema_genetico.fitness(mejor_cr)) 

def print_nono(sol):
    p = []
    print("Con valor fitness {}".format(sol[1]))
    for f in range(len(sol[0])):
        p.append([])
        for c in range(len(sol[0][f])):
            p[f].append(sol[0][c][f])
    for i in range(len(p)):
        row = []
        for j in range(len(p[i])):
            if p[i][j] == 1:
                row.append('■  ')
            else:
                row.append('X  ')
        print("".join(row))
    


#Tests
    
def main():
    gen = [0,1]
    L = 5   #Tamaño de los individuos
    TSIZE = 4   #Tamaño del torneo
    NGEN = 10000    #Numero de generaciones
    PSIZE = 20      #Tamaño de la población
    PCRUZ = 0.5     #Probabilidad de cruce
    PMUT = 0.01  #Probabilidad de mutación (Se recomienda baja porque prueba en cada fila de la matriz)
    #NPROB = 5       #Numero de problemas a resolver (por si puedo usar una funcion que los genere resolubles)
    prob = ((((2,1),(5,),(1,1,1),(1,1),(1,3)),((5,),(2,),(4,),(2,1),(2,1))),(((3,),(3,1),(2,1),(1,1),(2,1)),((4,),(3,1),(2,2),(0,),(2,1))))#Los problemas
    for k in range(len(prob)):
        seconds = time.time()
        problema_nono = ProblemaGenetico(gen, fun_dec, fun_mutar, fun_cruzar_matriz, fun_fitness_nono, L, prob[k])
        sol = algoritmo_genetico(problema_nono,TSIZE, max, NGEN, PSIZE, PCRUZ, PMUT)
        seconds = time.time() - seconds
        print("\nSolución problema nonograna {} en {} segundos".format(k+1, seconds))
        print_nono(sol)
